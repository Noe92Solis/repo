create database if not exists bd1;
use bd1;

create table if not exists jefes(
id int not null primary key auto_increment,
nombre varchar(60) not null
)engine InnoDB;

create table if not exists empleados(
id int not null primary key auto_increment,
primer_nombre varchar(50) not null,
segundo_nombre varchar(50) not null,
primer_apellido varchar(50) not null,
segundo_apellido varchar(50) not null,
correo varchar(50) not null,
tel_fijo varchar(9) not null,
tel_movil varchar(9) not null,
jefe int not null,
constraint jefe foreign key (jefe) references jefes(id)
)engine InnoDB;